'use strict'

describe 'Controller: SyncCtrl', ->

  # load the controller's module
  beforeEach module 'eacgtestApp'

  SyncCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    SyncCtrl = $controller 'SyncCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(scope.awesomeThings.length).toBe 3
