'use strict'

describe 'Directive: scheduler', ->

  # load the directive's module
  beforeEach module 'eacgtestApp'

  scope = {}

  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()

  it 'should make hidden element visible', inject ($compile) ->
    element = angular.element '<scheduler></scheduler>'
    element = $compile(element) scope
    expect(element.text()).toBe 'this is the scheduler directive'
