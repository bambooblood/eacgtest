'use strict'

###*
 # @ngdoc directive
 # @name eacgtestApp.directive:scheduler
 # @description
 # # scheduler
###
angular.module('eacgtestApp')
  .directive('scheduler', ->
    template: '<div></div>'
    restrict: 'E'
    link: (scope, element, attrs) ->
      element.text 'this is the scheduler directive'
  )
