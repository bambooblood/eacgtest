'use strict'

###*
 # @ngdoc overview
 # @name eacgtestApp
 # @description
 # # eacgtestApp
 #
 # Main module of the application.
###
angular
  .module('eacgtestApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'firebase'
  ])
  .config ($routeProvider) ->
    $routeProvider
      .when '/',
        templateUrl: 'views/main.html'
        controller: 'MainCtrl'
      .when '/about',
        templateUrl: 'views/about.html'
        controller: 'AboutCtrl'
      .when '/sync',
        templateUrl: 'views/sync.html'
        controller: 'SyncCtrl'
      .otherwise
        redirectTo: '/'

