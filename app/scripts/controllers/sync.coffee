'use strict'

###*
 # @ngdoc function
 # @name eacgtestApp.controller:SyncCtrl
 # @description
 # # SyncCtrl
 # Controller of the eacgtestApp
###
angular.module 'eacgtestApp'
.controller 'SyncCtrl', ($scope, $firebase, $timeout, $log) ->

  ref = new Firebase 'https://eacgtest.firebaseio.com/'

  $scope.journal = $firebase(ref).$asArray()

  $scope.listUnique = (field) ->
    _.uniq _.map $scope.journal, (item) -> item[field]

  $scope.modes = ['manual', 'scheduled', 'event']

  $scope.mode = $scope.modes[0]

  $scope.progress = false

  $scope.synchronizations = []

  $scope.addSync = ->
    sync =
      date: $scope.listUnique('syncDate')[0]
      status: $scope.listUnique('status')[0]
      conflict: 'none'
      syncedElements: $scope.journal.length
    $scope.synchronizations.push sync
    $scope.lastSync = sync.date

  $scope.emulateSync = ->
    $scope.progress = 0
    _.each $scope.journal, (item, index) =>
      $timeout =>
        item.syncDate = Date.now()
        $scope.journal.$save($scope.journal.indexOf(item)).then =>
          $scope.progress = parseInt($scope.journal.indexOf(item) / $scope.journal.length * 100)
          if $scope.journal.length is $scope.journal.indexOf(item) + 1
            $scope.addSync()
            $scope.progress = false
      , 500
