'use strict'

###*
 # @ngdoc function
 # @name eacgtestApp.controller:AboutCtrl
 # @description
 # # AboutCtrl
 # Controller of the eacgtestApp
###
angular.module('eacgtestApp')
  .controller 'AboutCtrl', ($scope) ->
    $scope.awesomeThings = [
      'HTML5 Boilerplate'
      'AngularJS'
      'Karma'
    ]
